import { QuestionTypesEnum } from "../enums/QuestionTypesEnum";

export type Uuid = string;

export type QuizQuestions = Record<Uuid, QuizQuestion>;

export interface BaseQuestion {
  id: Uuid;
  type: QuestionTypesEnum;
  question: string;
}

export interface TextQuizQuestion extends BaseQuestion {
  id: Uuid;
  type: QuestionTypesEnum.text;
  question: Text;
  answer: string;
}

export interface ChoiceQuestion extends BaseQuestion {
  id: Uuid;
  type: QuestionTypesEnum.choice;
  question: Text;
  options: readonly string[];
  answer: number; // index in the options array (TODO make type)
}

export type QuizQuestion = TextQuizQuestion | ChoiceQuestion;

type Text = string;
