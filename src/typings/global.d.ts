type Nullable<T> = T | null | undefined;

type ReactReturnType = JSX.Element | null;
