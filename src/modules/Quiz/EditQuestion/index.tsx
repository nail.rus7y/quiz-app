import React, { FormEvent } from "react";
import { Box, Flex, Button, Stack } from "@chakra-ui/react";
import { CheckIcon } from "@chakra-ui/icons";
import { useStore } from "effector-react";
import { questionStoreApi } from "../../../models/question";
import { Question } from "./question";
import { questionTypesStoreApi, $questionTypesStore } from "../../../models/questionTypes";
import { questionModeStoreApi } from "../../../models/questionMode";
import { QuestionTypesEnum } from "../../../enums/QuestionTypesEnum";

export function EditQuestion(): ReactReturnType {
  const questionType = useStore($questionTypesStore);
  const onSaveQuestion = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    questionStoreApi.save();
  };

  return (
    <Box w="40%">
      <Flex>
        <Stack w="40px" direction="column">
          <Button
            h="40px"
            colorScheme="twitter"
            isActive={questionType === QuestionTypesEnum.text}
            onClick={() => questionTypesStoreApi.text()}
          >
            T
          </Button>
          <Button
            h="40px"
            isActive={questionType === QuestionTypesEnum.choice}
            onClick={() => questionTypesStoreApi.choice()}
          >
            <CheckIcon />
          </Button>
        </Stack>
        <Flex w="8px" />
        <Flex w="100%">
          <Box
            margin="4 0 4 4"
            p={4}
            w="100%"
            bg="white"
            borderRadius="10px"
            boxShadow="0px 0px 2px 0px rgba(34, 60, 80, 0.2)"
          >
            <form onSubmit={onSaveQuestion}>
              <Question />
              <Flex marginTop={2} justifyContent="flex-end">
                <Box width="8px" />
                <Button colorScheme="twitter" type="submit">
                  Save
                </Button>
                <Box width="8px" />
                <Button colorScheme="twitter" onClick={() => questionModeStoreApi.read()}>
                  Cancel
                </Button>
              </Flex>
            </form>
          </Box>
        </Flex>
      </Flex>
    </Box>
  );
}
