import React from "react";
import { FormAnswerInput } from "../../../../components/common/FormAnswerInput";
import { FormQuestionInput } from "../../../../components/common/FormQuestionInput";

export function EditTextQuestion(): ReactReturnType {
  return (
    <>
      <FormQuestionInput />
      <FormAnswerInput />
    </>
  );
}
