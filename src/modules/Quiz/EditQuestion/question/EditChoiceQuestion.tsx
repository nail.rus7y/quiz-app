import { FormControl, FormLabel, Input, Button, Flex } from "@chakra-ui/react";
import React, { ChangeEvent } from "react";
import { CloseIcon, AddIcon } from "@chakra-ui/icons";
import { useList } from "effector-react";
import { ChoiceQuestion } from "../../../../typings";
import { $choiceQuestionOptionsStore, choiceQuestionOptionsApi } from "../../../../models/question";
import { FormAnswerInput } from "../../../../components/common/FormAnswerInput";
import { FormQuestionInput } from "../../../../components/common/FormQuestionInput";

// TODO нужно создать эвент для OPTIONS который отдаст тебе старый вариант стейта и там ты его обновишь, без использования useStore
// TODO обновлять question store нужно, но нужно так же разделять ui и хранение данных?

function Option({ option, index }: { option: string; index: number }): ReactReturnType {
  const { change, remove } = choiceQuestionOptionsApi;
  const onRemove = (): number => remove(index);
  const onChange = (e: ChangeEvent<HTMLInputElement>): Partial<ChoiceQuestion> =>
    change({ value: e.target.value, index });

  return (
    <Flex alignItems="center" marginY="16px">
      <Flex w="100%">
        <Input value={option} onChange={onChange} />
      </Flex>
      <Flex paddingLeft="16px">
        <CloseIcon
          onClick={onRemove}
          cursor="pointer"
          _hover={{
            opacity: 0.8,
          }}
        />
      </Flex>
    </Flex>
  );
}

function Options(): ReactReturnType {
  const { add } = choiceQuestionOptionsApi;
  const onAddOption = (): void => add();

  const optionsList = useList($choiceQuestionOptionsStore, {
    fn: (value, index) => <Option option={value} index={index} />,
  });

  return (
    <>
      <Button
        onClick={onAddOption}
        colorScheme="twitter"
        size="sm"
        variant="ghost"
        leftIcon={<AddIcon fontSize="14" />}
      >
        Add an option
      </Button>
      {optionsList}
    </>
  );
}

export function EditChoiceQuestion(): ReactReturnType {
  return (
    <>
      <FormQuestionInput />
      <FormControl id="options">
        <FormLabel paddingY={1}>Type an answer options</FormLabel>
        <Options />
      </FormControl>
      <FormAnswerInput />
    </>
  );
}
