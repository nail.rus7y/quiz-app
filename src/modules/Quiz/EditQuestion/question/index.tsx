import { useStore } from "effector-react";
import React from "react";
import { QuestionTypesEnum } from "../../../../enums/QuestionTypesEnum";
import { EditTextQuestion } from "./EditTextQuestion";
import { EditChoiceQuestion } from "./EditChoiceQuestion";
import { $questionTypesStore } from "../../../../models/questionTypes";

export const Question = (): ReactReturnType => {
  const questionType = useStore($questionTypesStore);

  switch (questionType) {
    case QuestionTypesEnum.text:
      return <EditTextQuestion />;
    case QuestionTypesEnum.choice:
      return <EditChoiceQuestion />;
    default: {
      const exhaustiveCheck: never = questionType;
      return exhaustiveCheck;
    }
  }
};
