import React from "react";

import { ViewTextQuestion } from "./ViewTextQuestion";
import { ViewChoiceQuestion } from "./ViewChoiceQuestion";
import { QuestionTypesEnum } from "../../../../enums/QuestionTypesEnum";
import { QuizQuestion } from "../../../../typings";

export const Question = ({ question }: { question: QuizQuestion }): ReactReturnType => {
  switch (question.type) {
    case QuestionTypesEnum.text:
      return <ViewTextQuestion question={question} />;
    case QuestionTypesEnum.choice:
      return <ViewChoiceQuestion question={question} />;
    default: {
      const exhaustiveCheck: never = question;
      return exhaustiveCheck;
    }
  }
};
