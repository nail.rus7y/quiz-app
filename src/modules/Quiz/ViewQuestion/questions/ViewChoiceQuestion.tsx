import React from "react";
import { Text, Flex } from "@chakra-ui/react";
import { ChoiceQuestion } from "../../../../typings";

interface Props {
  question: ChoiceQuestion;
}

export function ViewChoiceQuestion({ question }: Props): ReactReturnType {
  return (
    <>
      <Flex direction="column">
        <Text fontWeight="bold">Question</Text>
        <Text>{question.question}</Text>
      </Flex>
      <Flex direction="column">
        <Text fontWeight="bold">Options</Text>
        <Text>{question.options.join(", ")}</Text>
      </Flex>
      <Flex direction="column">
        <Text fontWeight="bold">Answer</Text>
        <Text>{question.answer}</Text>
      </Flex>
    </>
  );
}
