import React from "react";
import { Text, Flex } from "@chakra-ui/react";
import { TextQuizQuestion } from "../../../../typings";

interface Props {
  question: TextQuizQuestion;
}

export function ViewTextQuestion({ question }: Props): ReactReturnType {
  return (
    <>
      <Flex direction="column">
        <Text fontWeight="bold">Question</Text>
        <Text>{question.question}</Text>
      </Flex>
      <Flex direction="column">
        <Text fontWeight="bold">Answer</Text>
        <Text>{question.answer}</Text>
      </Flex>
    </>
  );
}
