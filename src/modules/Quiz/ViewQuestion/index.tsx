import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import { questionStoreApi } from "../../../models/question";
import { QuizQuestion } from "../../../typings";
import { Question } from "./questions";

interface ViewQuestionProps {
  question: QuizQuestion;
}

export function ViewQuestion({ question }: ViewQuestionProps): ReactReturnType {
  return (
    <Box
      onClick={() => questionStoreApi.edit(question)}
      p={4}
      m={4}
      w="40%"
      bg="white"
      borderRadius="10px"
      boxShadow="0px 0px 2px 0px rgba(34, 60, 80, 0.2)"
    >
      <Flex direction="column">
        <Flex justify="flex-end">
          <DeleteIcon
            cursor="pointer"
            _hover={{
              opacity: 0.8,
            }}
            onClick={(e) => {
              e.stopPropagation();
              questionStoreApi.delete(question.id);
            }}
          />
        </Flex>
        <Question question={question} />
      </Flex>
    </Box>
  );
}
