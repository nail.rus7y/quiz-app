import { Button, Container } from "@chakra-ui/react";
import React from "react";
import { useStore } from "effector-react";
import { Question } from "./QuizQuestion";
import { EditQuestion } from "./EditQuestion";
import { questionStoreApi } from "../../models/question";
import { $questionsStore } from "../../models/questions";
import { QuestionModeEnum } from "../../enums/QuestionModeEnum";
import { $questionModeStore } from "../../models/questionMode";

export function Quiz(): ReactReturnType {
  const questionModeStore = useStore($questionModeStore);
  const questionsStore = useStore($questionsStore);
  const onAddQuestion = (): void => questionStoreApi.create();

  return (
    <Container centerContent maxW="100%">
      <Button disabled={questionModeStore === QuestionModeEnum.create} onClick={onAddQuestion}>
        Add question
      </Button>
      {Object.values(questionsStore).map((question) => (
        <Question key={question.id} question={question} />
      ))}
      {questionModeStore === QuestionModeEnum.create && <EditQuestion />}
    </Container>
  );
}
