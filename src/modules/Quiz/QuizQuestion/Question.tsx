import React from "react";
import { useStore } from "effector-react";
import { QuizQuestion } from "../../../typings";
import { $questionModeStore } from "../../../models/questionMode";
import { ViewQuestion } from "../ViewQuestion";
import { QuestionModeEnum } from "../../../enums/QuestionModeEnum";
import { EditQuestion } from "../EditQuestion";
import { $questionId } from "../../../models/question/ui";

interface QuizQuestionProps {
  question: QuizQuestion;
}

export function Question({ question }: QuizQuestionProps): ReactReturnType {
  const questionId = useStore($questionId);
  const questionModeStore = useStore($questionModeStore);
  const editMode = questionModeStore === QuestionModeEnum.edit && questionId === question.id;

  return editMode ? <EditQuestion /> : <ViewQuestion question={question} />;
}
