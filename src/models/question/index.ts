import { createApi, sample, createEffect } from "effector";
import { TextQuizQuestion, Uuid, QuizQuestion, ChoiceQuestion } from "../../typings";
import { getUuid } from "../../helpers/uuid";
import { quizDomain } from "../quiz";
import { QuestionTypesEnum } from "../../enums/QuestionTypesEnum";

export const $questionStore = quizDomain.createStore<QuizQuestion>({
  id: getUuid(),
  type: QuestionTypesEnum.text,
  question: "",
  answer: "",
});

export const questionStoreApi = createApi($questionStore, {
  save: (state) => state,
  create: () => ({
    type: QuestionTypesEnum.text,
    id: getUuid(),
    question: "",
    answer: "",
  }),
  delete: (state, id: Uuid) => state,
  edit: (state, question: QuizQuestion) => question,
  change: (state, question: Partial<QuizQuestion>) => ({ ...Object.assign(state, question) }), // TODO Какого черта?
});

export const questionTextApi = {
  change: questionStoreApi.change.prepend((question: Partial<TextQuizQuestion>) => question),
};

export const questionChoiceApi = {
  change: questionStoreApi.change.prepend((question: Partial<ChoiceQuestion>) => question),
};

export const $choiceQuestionOptionsStore = $questionStore.map((state) =>
  state.type === QuestionTypesEnum.choice ? state.options : []
);

export const choiceQuestionOptionsApi = createApi($choiceQuestionOptionsStore, {
  remove: (state, index: number) => state.filter((_, i) => i !== index),
  add: (state) => [...state, ""],
  change: (state, { value, index }) => state.map((_, i) => (i === index ? value : _)),
});

sample({
  clock: [choiceQuestionOptionsApi.remove, choiceQuestionOptionsApi.add, choiceQuestionOptionsApi.change],
  source: $choiceQuestionOptionsStore,
  fn: (options) => ({ options }),
  target: questionStoreApi.change,
});
