import { sample } from "effector";
import { questionsStoreApi } from "../questions";
import { questionStoreApi, $questionStore } from "./index";
import { questionModeStoreApi } from "../questionMode";

sample({
  clock: questionStoreApi.create,
  target: [questionModeStoreApi.create],
});

sample({
  clock: questionStoreApi.edit,
  target: [questionModeStoreApi.edit],
});

sample({
  clock: questionStoreApi.save,
  source: $questionStore,
  target: [questionsStoreApi.saveQuestion, questionModeStoreApi.read],
});

sample({
  clock: questionStoreApi.delete,
  target: questionsStoreApi.removeQuestion,
});
