// TODO отображение текущего вопроса
import { $questionStore } from "./index";

export const $questionId = $questionStore.map((state) => state.id);
export const $questionUi = $questionStore.map((state) => state.question);
export const $answerUi = $questionStore.map((state) => state.answer);
export const $currentQuestionType = $questionStore.map((state) => state.type);
