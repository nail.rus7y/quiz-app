import { createApi, sample } from "effector";
import { quizDomain } from "../quiz";
import { QuestionTypesEnum } from "../../enums/QuestionTypesEnum";
import { QuizQuestion, Uuid } from "../../typings";
import { ChoiceQuestionFactory } from "../../factories/QuestionFactory/ChoiceQuestionFactory";
import { TextQuestionFactory } from "../../factories/QuestionFactory/TextQuestionFactory";
import { $questionStore } from "../question";

export const $questionTypesStore = quizDomain.createStore<QuestionTypesEnum>(QuestionTypesEnum.choice);
export const questionTypesStoreApi = createApi($questionTypesStore, {
  text: () => QuestionTypesEnum.text,
  choice: () => QuestionTypesEnum.choice,
});

const getQuestionFx = quizDomain.createEffect(({ type, id }: { type: QuestionTypesEnum; id: Uuid }): QuizQuestion => {
  switch (type) {
    case QuestionTypesEnum.choice:
      return new ChoiceQuestionFactory().getQuestion(id);
    case QuestionTypesEnum.text:
      return new TextQuestionFactory().getQuestion(id);
    default: {
      const exhaustiveCheck: never = type;
      return exhaustiveCheck;
    }
  }
});

sample({
  clock: $questionTypesStore,
  source: $questionStore,
  fn: (question, type) => ({ type, id: question.id }),
  target: getQuestionFx,
});

sample({
  clock: getQuestionFx.doneData,
  target: $questionStore,
});
