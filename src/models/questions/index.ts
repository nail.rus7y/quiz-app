import { createApi } from "effector";
import { QuizQuestion, QuizQuestions, Uuid } from "../../typings";
import { quizDomain } from "../quiz";
import { QuestionTypesEnum } from "../../enums/QuestionTypesEnum";
import { getUuid } from "../../helpers/uuid";

const id1 = getUuid();
const id2 = getUuid();

export const $questionsStore = quizDomain.createStore<QuizQuestions>({
  [id1]: {
    id: id1,
    type: QuestionTypesEnum.text,
    question: "What is the diameter of the Earth?",
    answer: "nobody knows",
  },
  [id2]: {
    id: id2,
    type: QuestionTypesEnum.choice,
    question: "Who will win?",
    options: ["Shark", "Bear"],
    answer: 0,
  },
});

export const questionsStoreApi = createApi($questionsStore, {
  saveQuestion: (state: QuizQuestions, question: QuizQuestion) => ({ ...state, [question.id]: question }),
  removeQuestion: (state: QuizQuestions, id: Uuid) =>
    Object.fromEntries(Object.entries<QuizQuestion>(state).filter(([key, value]) => value.id !== id)),
});
