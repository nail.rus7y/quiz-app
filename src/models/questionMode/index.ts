import { createApi } from "effector";
import { QuestionModeEnum } from "../../enums/QuestionModeEnum";
import { quizDomain } from "../quiz";

export const $questionModeStore = quizDomain.createStore<QuestionModeEnum>(QuestionModeEnum.read);
export const questionModeStoreApi = createApi($questionModeStore, {
  edit: () => QuestionModeEnum.edit,
  create: () => QuestionModeEnum.create,
  read: () => QuestionModeEnum.read,
});
