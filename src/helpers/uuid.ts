import { v4 } from "uuid";
import { Uuid } from "../typings";

export function getUuid(): Uuid {
  return v4() as Uuid;
}
