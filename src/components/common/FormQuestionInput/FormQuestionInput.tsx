import React from "react";
import { useStore } from "effector-react";
import { FormControl, FormLabel, Input } from "@chakra-ui/react";
import { $questionUi } from "../../../models/question/ui";
import { questionChoiceApi } from "../../../models/question";

export const FormQuestionInput = (): ReactReturnType => {
  const question = useStore($questionUi);

  return (
    <FormControl id="question">
      <FormLabel paddingY={1}>Type a question</FormLabel>
      <Input
        type="question"
        onChange={(e) => questionChoiceApi.change({ question: e.target.value })}
        value={question}
        placeholder="Question"
      />
    </FormControl>
  );
};
