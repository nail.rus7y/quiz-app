import React from "react";
import { useStore } from "effector-react";
import { FormControl, FormLabel, Input } from "@chakra-ui/react";
import { $answerUi } from "../../../models/question/ui";
import { questionChoiceApi } from "../../../models/question";

export const FormAnswerInput = (): ReactReturnType => {
  const answer = useStore($answerUi);

  return (
    <FormControl id="answer">
      <FormLabel paddingY={1}>Type an answer</FormLabel>
      <Input
        type="question"
        onChange={(e) => questionChoiceApi.change({ answer: Number(e.target.value) })}
        value={answer}
        placeholder="Question"
      />
    </FormControl>
  );
};
