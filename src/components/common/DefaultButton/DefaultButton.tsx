import React, { PropsWithChildren } from "react";
import { Button } from "@chakra-ui/react";

interface DefaultButtonProps {
  type?: string;
}

export function DefaultButton({ children }: PropsWithChildren<DefaultButtonProps>): ReactReturnType {
  return <Button>{children}</Button>;
}
