import { QuizQuestion, Uuid } from "../../typings";

export abstract class QuestionFactory {
  abstract getQuestion(id: Uuid): QuizQuestion;
}
