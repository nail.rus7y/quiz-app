import { ChoiceQuestion, Uuid } from "../../typings";
import { QuestionTypesEnum } from "../../enums/QuestionTypesEnum";
import { QuestionFactory } from "./index";

export class ChoiceQuestionFactory implements QuestionFactory {
  public getQuestion(id: Uuid): ChoiceQuestion {
    return {
      id,
      question: "",
      options: [],
      answer: 0,
      type: QuestionTypesEnum.choice,
    };
  }
}
