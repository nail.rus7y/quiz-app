import { TextQuizQuestion, Uuid } from "../../typings";
import { QuestionTypesEnum } from "../../enums/QuestionTypesEnum";
import { QuestionFactory } from "./index";

export class TextQuestionFactory implements QuestionFactory {
  public getQuestion(id: Uuid): TextQuizQuestion {
    return {
      id,
      question: "",
      answer: "",
      type: QuestionTypesEnum.text,
    };
  }
}
