import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { Quiz } from "./modules/Quiz";

export function App(): ReactReturnType {
  return (
    <Flex minHeight="100vh">
      <Box bgGradient="linear(red.100 0%, orange.100 25%, yellow.100 50%)" w="100%">
        <Quiz />
      </Box>
    </Flex>
  );
}
