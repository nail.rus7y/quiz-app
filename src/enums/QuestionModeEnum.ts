export enum QuestionModeEnum {
  create = "create",
  edit = "edit",
  read = "read",
}
